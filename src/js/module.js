(function () {
    $(document).ready(function () {
        var nav = $('.navbar');
        var banner_height = $('.top-banner').height();
        var navbar_height = $('.navbar').height();

        function scrollEffects(element) {
            $.each(element, function () {
                var h = $(this).offset().top - navbar_height;
                var attr = $(this).attr('id');
                var height = $(window).scrollTop();
                if (height >= h) {
                    $('.navbar-item[href="#' + attr + '"]').siblings().removeClass('active');
                    $('.navbar-item[href="#' + attr + '"]').addClass('active');
                } else if (height < banner_height) {
                    $('.navbar-item').removeClass('active');
                }
            });
        }
        $(window).scroll(function () {
            var height = $(this).scrollTop();
            if ($(window).width() > 1024) {
                (height > 0) ? nav.addClass('scrollable'): nav.removeClass('scrollable');
            }
            scrollEffects($('section'));
        });
        /* Form validation*/

        $('#feedback').on('submit', function () {
            var fields = $(this).find('.field');
            fields.each(function () {
                var str_length = $(this).find('.input').val();
                if (str_length.length == 0) {
                    $(this).addClass('error');
                    $('.help.last').show();
                }
                if ($('.checkbox input').attr('checked') != 'checked') {
                    $('.control.checkbox').addClass('error');
                }
            });
        });
        /* Label animation */

        var fields = $('#feedback').find('.field');
        fields.each(function () {
            var field = $(this).find('.input, textarea');
            field.on('change', function () {
                var str_length = $(this).val();
                (str_length.length > 0) ? $(this).next('.label').addClass('has-text'): $(this).next('.label').removeClass('has-text');
            });
        });
        $('.navbar-item').on('click', function () {
            var attr = $(this).attr('href');
            var section = $('' + attr + '');
            var height = section.offset().top - navbar_height / 2;
            if($(window).width()<767){
                height = section.offset().top;
            }
            $('html,body').animate({
                scrollTop: height
            }, 500);

        });

        /* Adaptive */

        $('.navbar-burger').click(function () {
            $('.navbar-burger').toggleClass('is-active');
            $('.navbar-menu,.navbar-brand').fadeToggle(300);
        });

        /* Animation */

        var wow = new WOW({
            boxClass: 'wow',
            animateClass: 'animated',
            offset: 0,
            mobile: false,
            live: true,
            callback: function (box) {},
        });
        wow.init();
    });
})();